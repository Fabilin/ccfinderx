cmake_minimum_required(VERSION 3.16)
project(ccfinderx)

set(CMAKE_CXX_STANDARD 14)

include_directories(ccfx)
include_directories(common)
include_directories(newengine)
include_directories(repdet)
include_directories(threadqueue)
include_directories(torq)
include_directories(torq/easytorq)
find_library(ICU libicuuc.so)
find_library(PTHREAD pthread)
find_library(BOOST_THREAD boost_thread)

add_executable(ccfinderx
        ccfx/ccfx.cpp
        ccfx/ccfxcommon.cpp
        ccfx/ccfxcommon.h
        ccfx/ccfxconstants.cpp
        ccfx/ccfxconstants.h
        ccfx/clonedataassembler.h
        ccfx/filteringmain.h
        ccfx/findfilemain.h
        ccfx/metricmain.h
        ccfx/preprocessorinvoker.h
        ccfx/prettyprintermain.cpp
        ccfx/prettyprintmain.h
        ccfx/rawclonepairdata.cpp
        ccfx/rawclonepairdata.h
        ccfx/shapedfragmentcalculator.h
        ccfx/transformermain.h
        common/allocaarray.h
        common/argvbuilder.h
        common/base64encoder.cpp
        common/base64encoder.h
        common/bitcounttable.h
        common/bitvector.cpp
        common/bitvector.h
        common/common.h
        common/datastructureonfile.h
        common/ffuncrenamer.h
        common/filestructwrapper.h
        common/hash_map_includer.h
        common/hash_set_includer.h
        common/specialstringmap.h
        common/unportable.cpp
        common/unportable.h
        common/utf8support.cpp
        common/utf8support.h
        newengine/clonedetector.h
        repdet/repdet.h
        threadqueue/threadqueue.h)
target_link_libraries(ccfinderx PRIVATE "${ICU}" "${PTHREAD}" "${BOOST_THREAD}")
target_compile_definitions(ccfinderx PUBLIC
        OS_UBUNTU
        LITTLE_ENDIAN
        CXLM_NO_ENC
        CODE_CONVERSION_SUPPORT
        WSTRING_CONVERSION_SUPPORT
        REQUIRE_RNR)

add_executable(picosel
        picosel/picosel.cpp
        common/unportable.cpp
        common/unportable.h)

add_executable(easytorq
        torq/interpreter.cpp
        torq/interpreter.h
        torq/texttoken.cpp
        torq/texttoken.h
        torq/torq.cpp
        torq/torqcommon.cpp
        torq/torqcommon.h
        torq/easytorq/easytorq.cpp
        torq/easytorq/easytorq.h
        torq/pyeasytorq/pyeasytorq.cpp
        common/unportable.cpp
        common/unportable.h
        common/utf8support.cpp
        common/utf8support.h)